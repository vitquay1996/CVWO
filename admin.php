<?php

//include the header
require_once('config/config.php');
// $username = stripslases($phpro_username);
if(isset($_GET['delpost'])){ 
	$db = new PDO("mysql:host=localhost; dbname=phpro_auth", 'root', 'doomboy1996');
	$stmt = $db->prepare('DELETE FROM blog_posts WHERE postID = :postID') ;
	$stmt->execute(array(':postID' => $_GET['delpost']));

	header('Location: admin.php?action=deleted');
	exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Blogie</title>

	<!-- Bootstrap Core CSS -->
	<link href="static/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="static/css/blog-post.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script language="JavaScript" type="text/javascript">
    function delpost(id, title)
    {
    	if (confirm("Are you sure you want to delete '" + title + "'"))
    	{
    		window.location.href = 'admin.php?delpost=' + id;
    	}
    }
    </script>
    <style>
    table {
    	border-collapse: collapse;
    	width: 100%;
    }

    th, td {
    	text-align: left;
    	padding: 8px;
    }

    tr:nth-child(even){background-color: #f2f2f2}
    </style>
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Blogie</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="discover.php">Discover</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="admin.php"><?php echo $phpro_username?></a>
                        </li>
                        <li>
                            <a href="model/addpost.php">New Post</a>
                        </li>
                        <li>
                            <a href="model/logout.php"><span class="glyphicon glyphicon-log-out"></span> Get Out</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>

            <!-- /.container -->
        </nav>
	<br>
	<br>
	<br>
	<br>
	<?php 
     //show message from add / edit page
	if(isset($_GET['action'])){ 
		echo '<h3>Post '.$_GET['action'].'.</h3>'; 
	} 
	?>
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<table>
				<tr>
					<th>Title</th>
					<th>Date</th>
					<th>Action</th>
				</tr>
				<?php
				try {
					$db = new PDO("mysql:host=localhost; dbname=phpro_auth", 'root', 'doomboy1996');
					$stmt = $db->query("SELECT postID, postTitle, postDate FROM blog_posts WHERE postAuthor= '$phpro_username' ORDER BY postID DESC");
					while($row = $stmt->fetch()){

						echo '<tr>';
						echo '<td>'.$row['postTitle'].'</td>';
						echo '<td>'.date('jS M Y', strtotime($row['postDate'])).'</td>';
						?>

						<td>
							<a href="edit-post.php?id=<?php echo $row['postID'];?>">Edit</a> | 
							<a href="javascript:delpost('<?php echo $row['postID'];?>','<?php echo $row['postTitle'];?>')">Delete</a>
						</td>

						<?php 
						echo '</tr>';

					}
				}
				catch(PDOException $e) {
					echo $e->getMessage();
				}
				?>
			</table>
		</div>
		<div class="col-sm-1"></div>

	</body>
	</html>