<?php

/*** begin the session ***/
session_start();

if(!isset($_SESSION['user_id']))
{	
	$message = 'Pls Log In';
	echo "<script type='text/javascript'>alert('$message');
	window.location.replace(\"index.html\");</script>";

}
else
{
    try
    {
        /*** connect to database ***/
        /*** mysql hostname ***/
        $mysql_hostname = 'localhost';

        /*** mysql username ***/
        $mysql_username = 'root';

        /*** mysql password ***/
        $mysql_password = 'doomboy1996';

        /*** database name ***/
        $mysql_dbname = 'phpro_auth';


        /*** select the users name from the database ***/
        $dbh = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname", $mysql_username, $mysql_password);
        /*** $message = a message saying we have connected ***/

        /*** set the error mode to excptions ***/
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        /*** prepare the insert ***/
        $stmt = $dbh->prepare("SELECT phpro_username FROM phpro_users 
            WHERE phpro_user_id = :phpro_user_id");

        /*** bind the parameters ***/
        $stmt->bindParam(':phpro_user_id', $_SESSION['user_id'], PDO::PARAM_INT);

        /*** execute the prepared statement ***/
        $stmt->execute();

        /*** check for a result ***/
        $phpro_username = $stmt->fetchColumn();

        /*** if we have no something is wrong ***/
        if($phpro_username == false)
        {
            $message = 'Access Error! Pls re-enter username and password';
        	echo "<script type='text/javascript'>alert('$message');
			window.location.replace(\"../index.html\");</script>";
        }
    }
    catch (PDOException $e)
    {
        /*** if we are here, something is wrong in the database ***/
        echo $e->getMessage();
    }
}

date_default_timezone_set('Asia/Singapore');
?>