<?php
//include config

require_once('../config/config.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Write a post</title>

	<!-- Bootstrap Core CSS -->
	<link href="../static/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="../static/css/blog-post.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js">
    </script>
    <script>
    tinymce.init({
    	selector: "textarea",
    	plugins: [
    	"advlist autolink lists link image charmap print preview anchor",
    	"searchreplace visualblocks code fullscreen",
    	"insertdatetime media table contextmenu paste"
    	],
    	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
    </script>     
</head>

<body>
	<br>
	<br>
	<br>
	<!-- Navigation -->

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../blog.php">Blogie</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li>
						<a href="#">About</a>
					</li>
					<li>
						<a href="#">Services</a>
					</li>
					<li>
						<a href="#">Contact</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="#"><?php echo $phpro_username?></a>
					</li>
					<li>
						<a href="model/logout.php"><span class="glyphicon glyphicon-log-out"></span> Get Out</a>
					</li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>
	<div class="row">
		<div class="col-sm-1">
		</div>
		<div class="col-sm-10">
			<form action='' method='post'>

				<p><label>Title</label><br />
					<input type='text' name='postTitle' value='<?php if(isset($error)){ echo $_POST['postTitle'];}?>'>
				</p>

				<p><label>Description</label><br />
					<textarea name='postDesc' cols='60' rows='10'><?php if(isset($error)){ echo $_POST['postDesc'];}?></textarea>
				</p>

				<p><label>Content</label><br />
					<textarea name='postCont' cols='60' rows='10'><?php if(isset($error)){ echo $_POST['postCont'];}?></textarea>
				</p>

				<p><button type='submit' class="btn btn-success" name='submit'>Post</button></p>

			</form>
		</div>
		<div class="col-sm-1">
		</div>
	</div>
</body>
</html>

<?php
if(isset($_POST['submit'])){
//Apply stripslashes to all elements in array
	$_POST = array_map('stripslashes', $_POST);

//collect form data so we have $postTitle, $postDesc, $postCont
	extract($_POST);

//very basic validation
	if($postTitle ==''){
		$error[] = 'Please enter the title.';
	}

	if($postDesc ==''){
		$error[] = 'Please enter the description.';
	}

	if($postCont ==''){
		$error[] = 'Please enter the content.';
	}

//put into database
	if(!isset($error)){

		try {

//insert into database
			$db = new PDO("mysql:host=localhost; dbname=phpro_auth", 'root', 'doomboy1996');
			$stmt = $db->prepare('INSERT INTO blog_posts (postTitle,postDesc,postCont,postDate,postAuthor) VALUES (:postTitle, :postDesc, :postCont, :postDate, :postAuthor)') ;
			$stmt->execute(array(
				':postTitle' => $postTitle,
				':postDesc' => $postDesc,
				':postCont' => $postCont,
				':postDate' => date('Y-m-d H:i:s'),
				':postAuthor' => $phpro_username
				));

//redirect to index page
			header('Location: ../blog.php');
			exit;

		} catch(PDOException $e) {
			echo $e->getMessage();
		}
	}
	else{
		foreach($error as $error){
			echo '<p class="error">'.$error.'</p>';
		}
	}

}
?>