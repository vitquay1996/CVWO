
<?php

/*** begin our session ***/
session_start();

/*** set a form token ***/
$form_token = md5( uniqid('auth', true) );

/*** set the session form token ***/
$_SESSION['form_token'] = $form_token;


?>


<script type="text/javascript">

function checkForm(form)
{
  if(form.phpro_username.value == "") {
    alert("Error: Username cannot be blank!");
    form.phpro_username.focus();
    return false;
  }
  re = /^\w+$/;
  if(!re.test(form.phpro_username.value)) {
    alert("Error: Username must contain only letters, numbers and underscores!");
    form.phpro_username.focus();
    return false;
  }

  if(form.phpro_password.value != "" && form.phpro_password.value == form.phpro_password2.value) {
    if(form.phpro_password.value.length < 6) {
      alert("Error: Password must contain at least six characters!");
      form.phpro_password.focus();
      return false;
    }
    if(form.phpro_password.value == form.phpro_username.value) {
      alert("Error: Password must be different from Username!");
      form.phpro_password.focus();
      return false;
    }
    re = /[0-9]/;
    if(!re.test(form.phpro_password.value)) {
      alert("Error: Password must contain at least one number (0-9)!");
      form.phpro_password.focus();
      return false;
    }
    re = /[a-z]/;
    if(!re.test(form.phpro_password.value)) {
      alert("Error: Password must contain at least one lowercase letter (a-z)!");
      form.phpro_password.focus();
      return false;
      
    }
  } else {
    alert("Error: Please check that you've entered and confirmed your password!");
    form.phpro_password.focus();
    return false;
  }

  return true;
}

</script>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Sign Up</title>

  <!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="../static/css/bootstrap.min.css" type="text/css">

  <!-- Custom Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="../static/css/font-awesome.min.css" type="text/css">

  <!-- Plugin CSS -->
  <link rel="stylesheet" href="../static/css/animate.min.css" type="text/css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="../static/css/creative.css" type="text/css">


</head>

<body id="page-top">

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="../index.html" class="navbar-brand">Blogie</a>
      </div>

    </div>
  </nav>  


  <header>
    <div class="header-content">
      <div class="header-content-inner">
        <h2>Sign Up</h2>
        <hr>
      </div>
      <form action="adduser_submit.php" method="post" onsubmit="return checkForm(this);">
        <p>
          <label for="phpro_username">Username</label>
          <input type="text" id="phpro_username" style="background-color: rgba(0, 0, 0, 0.5);" name="phpro_username" value="" maxlength="20" />
        </p>
        <p>
          <label for="phpro_password">Password</label>
          <input type="password" id="phpro_password" style="background-color: rgba(0, 0, 0, 0.5);" name="phpro_password" value="" maxlength="20" />
        </p>
        <p>
          <lable for="phpro_password2">Re-enter Password</label>
            <input type="password" id="phpro_password2" style="background-color: rgba(0, 0, 0, 0.5);" name="phpro_password2" value="" maxlenght"20" />
          </p>
          <p>
            <input type="hidden" name="form_token" value="<?php echo $form_token; ?>" />
            <button type="submit" class="btn btn-success">Submit</button>
          </p>
      </form>
    </div>
  </header>


</body>

</html>
