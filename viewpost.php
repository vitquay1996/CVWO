<?php

/*** begin the session ***/
session_start();

try
{
    /*** connect to database ***/
    /*** mysql hostname ***/
    $mysql_hostname = 'localhost';

    /*** mysql username ***/
    $mysql_username = 'root';

    /*** mysql password ***/
    $mysql_password = 'doomboy1996';

    /*** database name ***/
    $mysql_dbname = 'phpro_auth';


    /*** select the users name from the database ***/
    $dbh = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname", $mysql_username, $mysql_password);
    /*** $message = a message saying we have connected ***/

    /*** set the error mode to excptions ***/
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /*** prepare the insert ***/
    $stmt = $dbh->prepare("SELECT phpro_username FROM phpro_users 
        WHERE phpro_user_id = :phpro_user_id");

    /*** bind the parameters ***/
    $stmt->bindParam(':phpro_user_id', $_SESSION['user_id'], PDO::PARAM_INT);

    /*** execute the prepared statement ***/
    $stmt->execute();

    /*** check for a result ***/
    $phpro_username = $stmt->fetchColumn();

    /*** if we have no something is wrong ***/
    if($phpro_username == false)
    {
        $message = 'Access Error';
    }
    else
    {
        $message = 'Welcome '.$phpro_username;
    }
}
catch (Exception $e)
{
    /*** if we are here, something is wrong in the database ***/
    $message = 'We are unable to process your request. Please try again later"';
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blogie</title>

    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="blog.php">Blogie</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#">About</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                    </ul>
                    <?php
                    if($phpro_username != ""): ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="admin.php"><?php echo $phpro_username?></a>
                        </li>
                        <li>
                            <a href="model/addpost.php">New Post</a>
                        </li>
                        <li>
                            <a href="model/logout.php"><span class="glyphicon glyphicon-log-out"></span> Get Out</a>
                        </li>
                    </ul>
                <?php else: ?>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="model/adduser.php">Sign up</a>
                    </li>
                </ul>
                <?php endif;
                ?>
                
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-md-8">

                <!-- Blog Post -->
                <br>
                <br>
                <br>

                <?php
                        // connect to database
                /*** select the users name from the database ***/
                try {
                    $dbh = new PDO("mysql:host=localhost;dbname=phpro_auth", 'root', 'doomboy1996');

                    /*** set the error mode to excptions ***/
                    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                    /*** prepare the statment ***/
                    $stmt = $dbh->prepare("SELECT postID, postTitle, postCont, postDate FROM blog_posts WHERE postID = :postID");
                    $stmt->execute(array(':postID' => $_GET['id']));
                    $row = $stmt->fetch();
                    if($row['postID'] == ''){
                        header('Location: ./');
                        exit;
                    }
                    echo '<div>';
                            //title
                    echo '<h1><a href="viewpost.php?id='.$row['postID'].'">'.$row['postTitle'].'</a></h1>';
                    echo '<hr>';
                            //time
                    echo '<p><span class="glyphicon glyphicon-time"></span> Posted on '.date('jS M Y H:i:s', strtotime($row['postDate'])).'</p>';
                    echo '<hr>';
                            //content
                    echo '<p>'.$row['postCont'].'</p>';                
                    echo '<hr>';
                    echo '</div>';             

                }
                catch (PDOException $e) {
                    echo $e->getMessage();
                }

                ?>
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <br>
                <br>
                <br>
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>

            </div>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Quang 2016</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>